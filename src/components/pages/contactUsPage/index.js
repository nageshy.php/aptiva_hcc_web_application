import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import ContactPageOne from "./contactPageOne";
import ContactPageTwo from "./contactPageTwo";
import ContactPageThree from "./contactPageThree";

export default function ContactUsPage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <ContactPageOne />
            </div>
            <div>
                <ContactPageTwo />
            </div>
            <div>
                <ContactPageThree />
            </div>
            <div>
                <FooterPage />
            </div>

        </div>
    )
}