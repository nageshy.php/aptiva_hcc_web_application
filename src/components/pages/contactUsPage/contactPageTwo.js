import React from "react";
import "./style.css";

export default function ContactPageTwo() {

    return (
        <div>
            <section className="contact-form-area padding-top-120 padding-bottom-120">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-6">
                            <div className="content-left wow fadeInLeft">
                                <div className="phone-call margin-bottom-30">
                                    <div className="icon">
                                        <span><i class="fa fa-phone"></i></span>
                                    </div>
                                    <div className="phone-number">
                                        <span>Support number</span>
                                        <h2>+1 7327514647</h2>
                                    </div>
                                </div>
                                <div className="address-content">
                                    <div className="row">
                                        <div className="col-lg-6" style={{ marginBottom: '10px' }}>
                                            <div className="address-column">
                                                <span><i className="fas fa-city"></i></span>
                                                <h4>USA</h4>
                                                <p>825 Georges Road, </p>
                                                <p>Suite 1 North Brunswick,</p>
                                                <p> NJ 08902</p>
                                                <p> <br />+1 7327514647</p>
                                                <p>info@aptivacorp.com</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6" style={{ marginBottom: '10px' }}>
                                            <div className="address-column">
                                                <span><i className="fas fa-city"></i></span>
                                                <h4>INDIA</h4>
                                                <p>N Heights, Ground Floor Wing-B, Plot No.12,Madhapur</p>
                                                <p>Hyderabad, Telangana-500081, India</p>
                                                <p>+91 40 48549322</p>
                                                <p>info@aptivacorp.com</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6" style={{ marginBottom: '10px' }}>
                                            <div className="address-column">
                                                <span><i className="fas fa-city"></i></span>
                                                <h4>DUBAI</h4>
                                                <p>Oxford Tower – 608
                                                    P.O.Box No. 82264</p>
                                                <p>Dubai, UAE</p>
                                                <p>+ 971 4 4404832, + 971 4 4404955</p>
                                                <p>info@aptivacorp.com</p>
                                            </div>
                                        </div>
                                        <div className="col-lg-6" style={{ marginBottom: '10px' }}>
                                            <div className="address-column">
                                                <span><i className="fas fa-city"></i></span>
                                                <h4>ABU DHABI</h4>
                                                <p>Dar Al salaam Building
                                                    Office no:1016, 10th floor</p>
                                                <p>Liwa Corniche Street
                                                    Abu Dhabi, UAE</p>
                                                <p>+971 2 6267777</p>
                                                <p>info@aptivacorp.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6" style={{ marginTop: '24px' }}>
                            <div className="form-area wow fadeInRight">
                                <div className="title margin-bottom-30">
                                    <h2 style={{ fontWeight: "700" }}><span>Let's</span> Talk today</h2>
                                </div>
                                <form action="#">
                                    <div className="row">
                                        <div className="col-lg-12">
                                            <input type="text" placeholder="Name" />
                                        </div>
                                        <div className="col-lg-6">
                                            <input type="email" placeholder="Email Adress" />
                                        </div>
                                        <div className="col-lg-6">
                                            <input type="tel" placeholder="Phone Number" />
                                        </div>
                                        <div className="col-lg-12">
                                            <textarea rows="5" placeholder="Enter Your text"></textarea>
                                            <button type="submit" className="template-btn submit-btn pxp-section-cta pxp-section-cta-o margin-top-30">Send now</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}