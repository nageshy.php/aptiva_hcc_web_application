import React from "react";
import "./style.css";

export default function ContactPageThree() {

    return (
        <div>
            <div className="google-map padding-top-120 padding-bottom-120">
                <div className="container">
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="map-area">
                                {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3648.414686516678!2d90.39747551445822!3d23.874909589926506!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c43bc25ec0ad%3A0x92541def35db7c01!2sSoftTech-IT%20Institute!5e0!3m2!1sen!2sbd!4v1616050880839!5m2!1sen!2sbd" style={{ border: 0 }} allowfullscreen="" loading="lazy"></iframe> */}
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12141.811503120727!2d-74.45629700000002!3d40.465245!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c3c681c5e92ebb%3A0x8e7a757c0bd8d941!2s825%20Georges%20Rd%20%231%2C%20North%20Brunswick%20Township%2C%20NJ%2008902%2C%20USA!5e0!3m2!1sen!2sin!4v1646336619679!5m2!1sen!2sin" style={{ border: 0 }} allowfullscreen="" loading="lazy"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}