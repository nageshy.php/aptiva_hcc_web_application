import React from "react";
import "./style.css";

export default function ContactPageOne() {

    return (
        <div>
            <section className="pxp-page-header-simple" style={{ backgroundColor: "#ebf6ff" }}>
                <div className="pxp-container">
                    <h1 className="text-center">Contact us</h1>
                </div>
            </section>
            <section className="about-project padding-top-120 padding-bottom-120">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-lg-6">
                            <div className="left-content wow fadeInLeft">
                                <h2 className="margin-bottom-30"><span>Hello!</span> Tell us about your project service</h2>
                                <p className="margin-bottom-20"><i>Our vision is to inspire businesses to put employee wellbeing at the heart of everything they.</i></p>
                                <p>Consectetur elit. Vesti at bulum nec odio aea the dumm ipsumm ipsum that dolocons rsus mal suada and fadolorit to consectetur elit. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary.</p>
                            </div>
                        </div>
                        <div className="col-lg-6">
                            <div className="right-img wow fadeInRight">
                                <img src="https://html.softtechitltd.com/jeoguru/jeoguru/assets/images/abp-img.png" alt=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}