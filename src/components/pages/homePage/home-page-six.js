import React from "react";

export default function HomePageSix() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <h2 className="pxp-section-h2 text-center">Travel Jobs</h2>
                    <p className="pxp-text-light text-center">Search your career opportunity through 12,800 jobs</p>

                    <div className="row mt-4 mt-md-5 pxp-animate-i pxp-animate-i-to" style={{textAlign: 'initial'}}>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-bullhorn"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Marketing & Communication</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Senior Editor</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>San Francisco, CA
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Artistre Studio</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-1.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-calendar-o"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Project Management</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Software Engineering Team Leader</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>Los Angeles, CA
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Craftgenics</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-2.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-comments-o"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Customer Service</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Techincal Support Engineer</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>Paris, France
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Illuminate Studio</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-3.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-terminal"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Software Engineering</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Javascript Developer</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>London, UK
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Syspresoft</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-4.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-pie-chart"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Business Development</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Technical Writer</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>London, UK
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Gramware</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-5.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-address-card-o"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Human Resources</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Human Resources Coordinator</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>San Francisco, CA
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Bitbytech</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-6.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-terminal"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Software Engineering</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Fullstack Web Developer</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>New York, NY
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">CoderBotics</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-7.png)" }}></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-jobs-card-1-container">
                            <div className="pxp-jobs-card-1 pxp-has-border">
                                <div className="pxp-jobs-card-1-top">
                                    <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                        <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-bullhorn"></span></div>
                                        <div className="pxp-jobs-card-1-category-label">Marketing & Communication</div>
                                    </a>
                                    <a href="single-job-1.html" className="pxp-jobs-card-1-title">Social Media Expert</a>
                                    <div className="pxp-jobs-card-1-details">
                                        <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                            <span className="fa fa-globe"></span>San Francisco, CA
                                        </a>
                                        <div className="pxp-jobs-card-1-type">Full-time</div>
                                    </div>
                                </div>
                                <div className="pxp-jobs-card-1-bottom">
                                    <div className="pxp-jobs-card-1-bottom-left">
                                        <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                        <a href="single-company-1.html" className="pxp-jobs-card-1-company">Artistre Studio</a>
                                    </div>
                                    <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-1.png)" }}></a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="mt-4 mt-md-5 pxp-animate-i pxp-animate-i-to">
                        <a href="jobs-list-1.html" className="btn rounded-pill pxp-section-cta">All Job Offers<span className="fa fa-angle-right"></span></a>
                    </div>
                </div>
            </section>
        </div>
    )
}