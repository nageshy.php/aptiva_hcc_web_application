import React from "react";

export default function HomePageFour() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <h2 className="pxp-section-h2 text-center">Benifits</h2>
                    <p className="pxp-text-light text-center">Start your next carrer in a beautiful Quality</p>

                    <div className="row mt-4 mt-md-5 pxp-animate-i pxp-animate-i-to">
                        <div className="col-12 col-md-12 col-lg-4 col-xxl-3 pxp-cities-card-1-container">
                            <a href="jobs-list-1.html" className="pxp-cities-card-1 text-center">
                                <div className="pxp-cities-card-1-top">
                                    <div className="pxp-cities-card-1-image pxp-cover" style={{ backgroundImage: "url(https://www.business.com/images/content/60e/865b77b4374c22d8b4567/617-591-)" }}></div>
                                    <div className="pxp-cities-card-1-name">Employee Benifits</div>
                                </div>
                                <div className="pxp-cities-card-1-bottom">
                                    <div className="pxp-cities-card-1-jobs">366 open positions</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                </div>
                            </a>
                        </div>
                        <div className="col-12 col-md-12 col-lg-4 col-xxl-3 pxp-cities-card-1-container">
                            <a href="jobs-list-1.html" className="pxp-cities-card-1 text-center">
                                <div className="pxp-cities-card-1-top">
                                    <div className="pxp-cities-card-1-image pxp-cover" style={{ backgroundImage: "url(https://www.consumercredit.com/wp-content/uploads/2020/05/employment-benefits-yf2.jpg)" }}></div>
                                    <div className="pxp-cities-card-1-name">Employee Benifits</div>
                                </div>
                                <div className="pxp-cities-card-1-bottom">
                                    <div className="pxp-cities-card-1-jobs">366 open positions</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                </div>
                            </a>
                        </div>
                        <div className="col-12 col-md-12 col-lg-4 col-xxl-3 pxp-cities-card-1-container">
                            <a href="jobs-list-1.html" className="pxp-cities-card-1 text-center">
                                <div className="pxp-cities-card-1-top">
                                    <div className="pxp-cities-card-1-image pxp-cover" style={{ backgroundImage: "url(https://blog.vantagecircle.com/content/images/2020/08/types-of-employee-benefits--1-.png)" }}></div>
                                    <div className="pxp-cities-card-1-name">Employee Benifits</div>
                                </div>
                                <div className="pxp-cities-card-1-bottom">
                                    <div className="pxp-cities-card-1-jobs">366 open positions</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                </div>
                            </a>
                        </div>
                        <div className="col-12 col-md-12 col-lg-4 col-xxl-3 pxp-cities-card-1-container">
                            <a href="jobs-list-1.html" className="pxp-cities-card-1 text-center">
                                <div className="pxp-cities-card-1-top">
                                    <div className="pxp-cities-card-1-image pxp-cover" style={{ backgroundImage: "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS0puRb--IsgrqzGXGcLvXs_bQ3leAcOwsFNC9AGO6uRqyCPIWCtTJu4g2R9C8pjlov3Jo&usqp=CAU)" }}></div>
                                    <div className="pxp-cities-card-1-name">Employee Benifits</div>
                                </div>
                                <div className="pxp-cities-card-1-bottom">
                                    <div className="pxp-cities-card-1-jobs">366 open positions</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                    <div className="pxp-cities-card-1-jobs">------------</div>
                                </div>
                            </a>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    )
}