import React from "react";

export default function HomePageThree() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <div className="row justify-content-between align-items-center mt-4 mt-md-5">
                        <h2 className="pxp-section-h2 text-center">Current Healthcare Job Opportunities</h2>
                        <p className="pxp-text-light text-center">Start your next carrer in a beautiful Category</p>
                        <div className="col-lg-6 col-xxl-5">
                            <div className="pxp-info-fig pxp-animate-i pxp-animate-i-righ">
                                <div className="pxp-info-fig-image pxp-cover" style={{ backgroundImage: "url(https://www.protouchstaffing.com/img/travel-staffing-1.svg)" }}></div>
                                <div className="pxp-info-stats" style={{ right: "56px" }}>
                                    <div className="pxp-info-stats-item pxp-animate-i pxp-animate-bounc">
                                        <div className="pxp-info-stats-item-number">Travel Jobs<span></span></div>

                                    </div>

                                    <div className="pxp-info-stats-item pxp-animate-i pxp-animate-bounc">

                                        <button className="btn rounded-pill pxp-section-cta" style={{ width: "-webkit-fill-available" }}>Browse </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-6 col-xxl-5">
                            <div className="pxp-info-fig pxp-animate-i pxp-animate-i-right">
                                <div className="pxp-info-fig-image pxp-cover" style={{ backgroundImage: "url(https://www.protouchstaffing.com/img/permanent-direct-hire-staffing-1.svg)" }}></div>
                                <div className="pxp-info-stats">
                                    <div className="pxp-info-stats-item pxp-animate-i pxp-animate-bounc">
                                        <div className="pxp-info-stats-item-number">Permanent Jobs  <span></span></div>

                                    </div>

                                    <div className="pxp-info-stats-item pxp-animate-i pxp-animate-bounc">

                                        <button className="btn rounded-pill pxp-section-cta" style={{ width: "-webkit-fill-available" }}>Browse </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </div>
    )
}