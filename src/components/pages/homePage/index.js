import React from "react";

import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import HomePageSectionOne from "./home-page-one";
import HomePageTwo from "./home-page-two";
import HomePageThree from "./home-page-three";
import HomePageFour from "./home-page-four";
import HomePageFive from "./home-page-five";
import HomePageSix from "./home-page-six";
import HomePageSeven from "./home-page-seven";
import HomePageEight from "./home-page-eight";
import HomePageNine from "./home-page-nine";



export default function HomePage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <HomePageSectionOne />
            </div>
            <div>
                <HomePageTwo />
            </div>
            <div>
                <HomePageThree />
            </div>
            <div>
                <HomePageFour />
            </div>
            <div>
                <HomePageFive />
            </div>
            <div>
                <HomePageSix />
            </div>
            <div>
                <HomePageSeven />
            </div>
            <div>
                <HomePageEight />
            </div>
            <div>
                <HomePageNine />
            </div>

            <div>
                <FooterPage />
            </div>

        </div>
    )
}