import React from "react";


export default function HomePageSectionOne() {

    return (
        <div>
            <section className="pxp-hero pxp-hero-boxed">
                <section className="hero-area">
                    <div id="carouselExampleTouch" className="carousel slide" data-mdb-touch="false">
                        <div className="carousel-indicators">
                            <button
                                type="button"
                                data-mdb-target="#carouselExampleTouch"
                                data-mdb-slide-to="0"
                                className="active"
                                aria-current="true"
                                aria-label="Slide 1"
                            ></button>
                            <button
                                type="button"
                                data-mdb-target="#carouselExampleTouch"
                                data-mdb-slide-to="1"
                                aria-label="Slide 2"
                            ></button>
                            <button
                                type="button"
                                data-mdb-target="#carouselExampleTouch"
                                data-mdb-slide-to="2"
                                aria-label="Slide 3"
                            ></button>
                        </div>
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img src="img/slides/aptiva1.jpg" className="d-block w-100" alt="Wild Landscape" />
                                <div className="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                            <div className="carousel-item">
                                <img src="img/slides/aptiva2.jpg" className="d-block w-100" alt="Camera" />
                                <div className="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                            <div className="carousel-item">
                                <img src="img/slides/aptiva3.jpg" className="d-block w-100" alt="Exotic Fruits" />
                                <div className="carousel-caption d-none d-md-block">
                                </div>
                            </div>
                        </div>
                        <button className="carousel-control-prev" type="button" data-mdb-target="#carouselExampleTouch" data-mdb-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-mdb-target="#carouselExampleTouch" data-mdb-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>
                    </div>

                </section>
            </section>
            <section className="mt-100 pxp-no-hero">
                <div className="pxp-container text-center">
                    <h2 className="pxp-section-h2 text-center" style={{ textTransform: "capitalize" }}>A joint commission accredited staffing agency</h2>
                    <img src="img/GoldSeal_Four_Colo.jpg" width="400px" />
                </div>
            </section>
        </div>
    )
}