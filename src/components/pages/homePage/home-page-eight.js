import React from "react";

export default function HomePageEight() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <h2 className="pxp-section-h2 text-center">Why Customers Love Us</h2>
                    <p className="pxp-text-light text-center">What our customers say about us</p>

                    <div className="pxp-testimonials-1">
                        <div className="pxp-testimonials-1-circles d-none d-md-block">
                            <div className="pxp-testimonials-1-circles-item pxp-item-1 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-1.png)" }}></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-2 pxp-animate-i pxp-animate-bounc"></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-3 pxp-animate-i pxp-animate-bounc"></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-4 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-2.png)" }}></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-5 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-3.png)" }}></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-6 pxp-animate-i pxp-animate-bounc"></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-7 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-4.png)" }}></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-8 pxp-animate-i pxp-animate-bounc"></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-9 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-5.png)" }}></div>
                            <div className="pxp-testimonials-1-circles-item pxp-item-10 pxp-cover pxp-animate-i pxp-animate-bounc" style={{ backgroundImage: "url(images/customer-6.png)" }}></div>
                        </div>

                        <div className="pxp-testimonials-1-carousel-container">
                            <div className="row justify-content-center align-items-center">
                                <div className="col-10 col-md-6 col-lg-6 col-xl-5 col-xxl-4">
                                    <div className="pxp-testimonials-1-carousel pxp-animate-i pxp-animate-i-top">
                                        <div id="pxpTestimonials1Carousel" className="carousel slide" data-bs-ride="carousel">
                                            <div className="carousel-inner">
                                                <div className="carousel-item text-center active">
                                                    <div className="pxp-testimonials-1-carousel-item-text">Aptiva Health Care is an ever-changing workplace, striving to innovate by bringing employers and candidates together. We are dedicated to improving our clients’ lives as well as our own employees.</div>
                                                    <div className="pxp-testimonials-1-carousel-item-name">Susanne Weil</div>
                                                    <div className="pxp-testimonials-1-carousel-item-company">Illuminate Studio</div>
                                                </div>
                                                <div className="carousel-item text-center">
                                                    <div className="pxp-testimonials-1-carousel-item-text">Each day, I’m inspired by my colleagues to drive innovation that accomplishes this. Aptiva Health Care fosters an environment of trust and support where I can drive customer success.</div>
                                                    <div className="pxp-testimonials-1-carousel-item-name">Kenneth Spiers</div>
                                                    <div className="pxp-testimonials-1-carousel-item-company">Syspresoft</div>
                                                </div>
                                                <div className="carousel-item text-center">
                                                    <div className="pxp-testimonials-1-carousel-item-text">Aptiva Health Care is an ever-changing workplace, striving to innovate by bringing employers and candidates together. We are dedicated to improving our clients’ lives as well as our own employees.</div>
                                                    <div className="pxp-testimonials-1-carousel-item-name">Rebecca Eason</div>
                                                    <div className="pxp-testimonials-1-carousel-item-company">Craftgenics</div>
                                                </div>
                                            </div>
                                            <button className="carousel-control-prev" type="button" data-bs-target="#pxpTestimonials1Carousel" data-bs-slide="prev">
                                                <span className="fa fa-angle-left" aria-hidden="true"></span>
                                                <span className="visually-hidden">Previous</span>
                                            </button>
                                            <button className="carousel-control-next" type="button" data-bs-target="#pxpTestimonials1Carousel" data-bs-slide="next">
                                                <span className="fa fa-angle-right" aria-hidden="true"></span>
                                                <span className="visually-hidden">Next</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}