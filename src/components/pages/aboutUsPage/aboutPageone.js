import React from "react";
import "./style.css";

export default function AboutPageOne() {

    return (
        <div>
             <section className="pxp-page-header-simple" style={{ backgroundColor: "#ebf6ff" }}>
                <div className="pxp-container">
                    <h1 className="text-center">About us</h1>
                    <p className="pxp-text-light text-center">We help employers and candidates find the right fit</p>

                </div>
            </section>
            <section className="pxp-page-image-hero pxp-cover" style={{ backgroundImage: "url(images/company-hero-3.jpg)" }}>
                <div className="pxp-hero-opacity"></div>
                <div className="pxp-page-image-hero-caption">
                    <div className="pxp-container">
                        <div className="row justify-content-center">
                            <div className="col-9 col-md-8 col-xl-7 col-xxl-6">
                                <h1 className="text-center">We help companies and candidates find the right fit</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}