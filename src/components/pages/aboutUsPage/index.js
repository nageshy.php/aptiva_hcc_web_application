import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import AboutPageOne from "./aboutPageone";
import AboutPageTwo from "./aboutPageTwo";
import AboutPageThree from "./aboutPageThree";
import AboutPageFour from "./aboutPageFour";
import AboutPageFive from "./aboutPageFive";
import AboutPageSix from "./aboutPageSix";


export default function AboutUs() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <AboutPageOne />
            </div>
            <div>
                <AboutPageFour />
            </div>
            <div>
                <AboutPageThree />
            </div>
            {/* <div>
                <AboutPageTwo />
            </div> */}
            <div>
                <AboutPageFive />
            </div>
            <div>
                <AboutPageSix />
            </div>
            <div>
                <FooterPage />
            </div>

        </div>
    )
}