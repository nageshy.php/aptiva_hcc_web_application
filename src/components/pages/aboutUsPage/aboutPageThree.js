import React from "react";
import "./style.css";

export default function AboutPageThree() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <h2 className="pxp-section-h2 text-center">We want to know more about you</h2>
                    <p className="pxp-text-light text-center">Select your profile</p>

                    <div className="row justify-content-evenly mt-4 mt-md-5 pxp-animate-i pxp-animate-in-top">
                        <div className="col-lg-4 col-xl-3 pxp-services-1-item-container">
                            <div className="pxp-services-1-item text-center pxp-animate-icon">
                                <div className="pxp-services-1-item-icon">
                                    <img src="images/service-1-icon.png" alt="Candidate" />
                                </div>
                                <div className="pxp-services-1-item-title">I am a candidate</div>
                                <div className="pxp-services-1-item-text pxp-text-light">You don’t want just any job, you want the right fit. Find it here.</div>
                                <div className="pxp-services-1-item-cta">
                                    <a href="about-us.html">Read more<span className="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-xl-3 pxp-services-1-item-container">
                            <div className="pxp-services-1-item text-center pxp-animate-icon">
                                <div className="pxp-services-1-item-icon">
                                    <img src="images/service-2-icon.png" alt="Employer" />
                                </div>
                                <div className="pxp-services-1-item-title">I am an employer</div>
                                <div className="pxp-services-1-item-text pxp-text-light">You don’t want just any candidate, you want the right fit.</div>
                                <div className="pxp-services-1-item-cta">
                                    <a href="about-us.html">Read more<span className="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-4 col-xl-3 pxp-services-1-item-container">
                            <div className="pxp-services-1-item text-center pxp-animate-icon">
                                <div className="pxp-services-1-item-icon">
                                    <img src="images/service-3-icon.png" alt="Press" />
                                </div>
                                <div className="pxp-services-1-item-title">I am a member of the press</div>
                                <div className="pxp-services-1-item-text pxp-text-light">See what is happening at aptiva.</div>
                                <div className="pxp-services-1-item-cta">
                                    <a href="about-us.html">Read more<span className="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}