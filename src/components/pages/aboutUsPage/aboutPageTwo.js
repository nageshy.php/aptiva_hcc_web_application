import React from "react";
import "./style.css";

export default function AboutPageTwo() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <div className="row justify-content-center">
                        <div className="col-xl-7 col-xxl-6">
                            {/* <h2 className="pxp-section-h2 text-center">About Us</h2> */}
                            {/* <p className="pxp-text-light text-center">We help employers and candidates find the right fit</p> */}
                            <div className="mt-4 mt-md-5 text-center">
                                <p>aptiva, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services.</p>
                                <p>The company is headquartered near Beaverton, Oregon, in the Portland metropolitan area.[3] It is the world's largest supplier of athletic shoes and apparel and a major manufacturer of sports equipment, with revenue in excess of US$37.4 billion in its fiscal year 2020 (ending May 31, 2020).[4] As of 2020, it employed 76,700 people worldwide.[5] In 2020 the brand alone was valued in excess of $32 billion, making it the most valuable brand among sports businesses.[6] Previously, in 2017, the aptiva brand was valued at $29.6 billion.[7] aptiva ranked 89th in the 2018 Fortune 500 list of the largest United States corporations by total revenue.[8]</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}