import React from "react";
import "./style.css";

export default function AboutPageFour() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <div className="row justify-content-between align-items-center mt-4 mt-md-5">
                        <div className="col-lg-6 col-xxl-5">
                            <div className="pxp-info-fig pxp-animate-i pxp-animate-in-right">
                                <div className="pxp-info-fig-image pxp-cover" style={{ backgroundImage: "url(images/info-section-image.jpg)" }}></div>
                                <div className="pxp-info-stats">
                                    <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounc">
                                        <div className="pxp-info-stats-item-number">130<span>job offers</span></div>
                                        <div className="pxp-info-stats-item-description">in Business Development</div>
                                    </div>
                                    <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounc">
                                        <div className="pxp-info-stats-item-number">480<span>job offers</span></div>
                                        <div className="pxp-info-stats-item-description">in Marketing & Communication</div>
                                    </div>
                                    <div className="pxp-info-stats-item pxp-animate-in pxp-animate-bounc">
                                        <div className="pxp-info-stats-item-number">312<span>job offers</span></div>
                                        <div className="pxp-info-stats-item-description">in Human Resources</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-5 col-xxl-6">
                            <div className="pxp-info-caption pxp-animate-i pxp-animate-in-top mt-4 mt-sm-5 mt-lg-0">
                                <h2 className="pxp-section-h2">Millions of jobs.<br />Find the one that suits you.</h2>
                                <p className="pxp-text-light">Search all the open positions on the web. Get your own personalized salary estimate. Read reviews on over 600,000 companies worldwide.</p>
                                <p>aptiva, Inc. is an American multinational corporation that is engaged in the design, development, manufacturing, and worldwide marketing and sales of footwear, apparel, equipment, accessories, and services.</p>
                                <p>The company is headquartered near Beaverton, Oregon, in the Portland metropolitan area.[3] It is the world's largest supplier of athletic shoes and apparel and a major manufacturer of sports equipment, with revenue in excess of US$37.4 billion in its fiscal year 2020 (ending May 31, 2020).[4] As of 2020, it employed 76,700 people worldwide.[5] In 2020 the brand alone was valued in excess of $32 billion, making it the most valuable brand among sports businesses.[6] Previously, in 2017, the aptiva brand was valued at $29.6 billion.[7] aptiva ranked 89th in the 2018 Fortune 500 list of the largest United States corporations by total revenue.[8]</p>
                                {/* <div className="pxp-info-caption-list">
                                    <div className="pxp-info-caption-list-item">
                                        <img src="images/check.svg" alt="-" /><span>Bring to the table win-win survival</span>
                                    </div>
                                    <div className="pxp-info-caption-list-item">
                                        <img src="images/check.svg" alt="-" /><span>Capitalize on low hanging fruit to identify</span>
                                    </div>
                                    <div className="pxp-info-caption-list-item">
                                        <img src="images/check.svg" alt="-" /><span>But I must explain to you how all this</span>
                                    </div>
                                </div> */}
                                {/* <div className="pxp-info-caption-cta">
                                    <a href="jobs-list-1.html" className="btn rounded-pill pxp-section-cta">Get Started Now<span className="fa fa-angle-right"></span></a>
                                </div> */}
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    )
}