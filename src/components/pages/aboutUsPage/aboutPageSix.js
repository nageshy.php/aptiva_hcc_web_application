import React from "react";
import "./style.css";

export default function AboutPageSix(){

    return(
        <div>
            <section className="mt-100">
            <div className="pxp-container">
                <h2 className="pxp-section-h2 text-center">Stay Up to Date</h2>
                <p className="pxp-text-light text-center">Subscribe to our newsletter to receive our weekly feed.</p>

                <div className="row mt-4 mt-md-5 justify-content-center">
                    <div className="col-md-9 col-lg-7 col-xl-6 col-xxl-5">
                        <div className="pxp-subscribe-1-container pxp-animate-i pxp-animate-in-top">
                            <div className="pxp-subscribe-1-image">
                                <img src="images/subscribe.png" alt="Stay Up to Date"/>
                            </div>
                            <div className="pxp-subscribe-1-form">
                                <form>
                                    <div className="input-group">
                                        <input type="text" className="form-control" placeholder="Enter your email..."/>
                                        <button className="btn btn-primary" type="button">Subscribe<span className="fa fa-angle-right"></span></button>
                                      </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        </div>
    )
}