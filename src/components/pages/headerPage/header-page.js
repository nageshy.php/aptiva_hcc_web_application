import React from "react";
import "./style.css";
import { useNavigate } from 'react-router';


export default function HeaderPage() {
    let navigate = useNavigate();

    function handleHome() {
        navigate('/')
    }

    function handleAbout() {
        navigate('/about-us')
    }

    function handleContact() {
        navigate('/contact-us')
    }
    function handleSignUp() {
        navigate('/sign-up')
    }
    function handleSignIn() {
        navigate('/sign-in')
    }
    function handleServices() {
        navigate('/services')
    }
    function handleCareers() {
        navigate('/careers')
    }
    function handleBlog() {
        navigate('/blog')
    }

    return (
        <div>
            <header className="pxp-header fixed-top pxp-bigger">
                <div className="pxp-container">
                    <div className="pxp-header-container">
                        <div className="pxp-logo">
                            <a role="button" onClick={handleHome} className="pxp-animate"><img src="img/logo.png" /></a>
                        </div>
                        <div className="pxp-nav-trigger navbar d-xl-none flex-fill">
                            <a role="button" data-bs-toggle="offcanvas" data-bs-target="#pxpMobileNav" aria-controls="pxpMobileNav">
                                <div className="pxp-line-1"></div>
                                <div className="pxp-line-2"></div>
                                <div className="pxp-line-3"></div>
                            </a>
                            <div className="offcanvas offcanvas-start pxp-nav-mobile-container" tabIndex="-1" id="pxpMobileNav">
                                <div className="offcanvas-header">
                                    <div className="pxp-logo">
                                        <a onClick={handleHome} className="pxp-animate"> <img src="img/logo.png" /></a>
                                    </div>
                                    <button type="button" className="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                                </div>
                                <div className="offcanvas-body">
                                    <nav className="pxp-nav-mobile">
                                        <ul className="navbar-nav justify-content-end flex-grow-1">
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleHome} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">Home</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleAbout} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">About Us</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleServices} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">Services</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleCareers} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">Careers</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleBlog} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">Blog</a>
                                            </li>
                                            <li className="nav-item dropdown">
                                                <a role="button" onClick={handleContact} className="nav-link dropdown-toggle" data-bs-toggle="dropdown">Contact Us</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="pxp-header-right">
                            <nav className="pxp-nav dropdown-hover-all d-none d-xl-block">
                                <ul>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleHome} className="dropdown-toggle" data-bs-toggle="dropdown">Home</a>
                                    </li>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleAbout} className="dropdown-toggle" data-bs-toggle="dropdown">About Us</a>

                                    </li>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleServices} className="dropdown-toggle" data-bs-toggle="dropdown">Services</a>

                                    </li>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleCareers} className="dropdown-toggle" data-bs-toggle="dropdown">Careers</a>

                                    </li>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleBlog} className="dropdown-toggle" data-bs-toggle="dropdown">Blog</a>

                                    </li>
                                    <li className="dropdown">
                                        <a role="button" onClick={handleContact} className="dropdown-toggle" data-bs-toggle="dropdown">Contact Us</a>

                                    </li>
                                </ul>
                            </nav>
                            <nav className="pxp-user-nav pxp-on-light d-none d-sm-flex">
                                <a className="btn rounded-pill pxp-nav-btn" data-bs-toggle="modal" onClick={handleSignIn} role="button">Log in</a>
                                <a className="btn rounded-pill pxp-nav-btn" data-bs-toggle="modal" onClick={handleSignUp} role="button">Registration</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
        </div>
    )
}