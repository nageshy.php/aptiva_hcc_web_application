import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import BlogPageOne from "./blogPageOne";

export default function BlogPage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <BlogPageOne />
            </div>
            <div>
                <FooterPage />
            </div>
        </div>
    )
}