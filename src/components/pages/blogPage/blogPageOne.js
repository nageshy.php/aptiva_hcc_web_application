import React from "react";
import "./style.css";
import { useNavigate } from 'react-router';

export default function BlogPageOne() {
    let navigate = useNavigate();

    function handleBlogDetails() {
        navigate('/blog-details')
    }
    return (
        <div>
            <section className="pxp-page-header-simple" style={{ backgroundColor: "#ebf6ff", marginBottom: '20px' }}>
                <div className="pxp-container">
                    <h1 className="text-center">Top Career Advice</h1>
                    <div className="pxp-hero-subtitle pxp-text-light">Browse the latest career advices</div>
                </div>
            </section>
            <section>
                <div className="pxp-container">
                    <div id="pxp-blog-featured-posts-carousel" className="carousel slide carousel-fade pxp-blog-featured-posts-carousel" data-bs-ride="carousel">
                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <div className="pxp-featured-posts-item pxp-cover" style={{ backgroundImage: "url(images/company-hero-5.jpg)" }}>
                                    <div className="pxp-hero-opacity"></div>
                                    <div className="pxp-featured-posts-item-caption">
                                        <div className="pxp-featured-posts-item-caption-content">
                                            <div className="row align-content-center justify-content-center">
                                                <div className="col-9 col-md-8 col-xl-7 col-xxl-6">
                                                    <div className="pxp-featured-posts-item-date">August 31, 2021</div>
                                                    <div className="pxp-featured-posts-item-title">10 awesome free career self assessments</div>
                                                    <div className="pxp-featured-posts-item-summary pxp-text-light mt-2">Figuring out what you want to be when you grow up is hard, but a career test can make it easier to find...</div>
                                                    <div className="mt-4 mt-md-5 text-center">
                                                        <a role="button" onClick={handleBlogDetails} className="btn rounded-pill pxp-section-cta">Read Article<span className="fa fa-angle-right"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="pxp-featured-posts-item pxp-cover" style={{ backgroundImage: "url(images/company-hero-3.jpg)" }}>
                                    <div className="pxp-hero-opacity"></div>
                                    <div className="pxp-featured-posts-item-caption">
                                        <div className="pxp-featured-posts-item-caption-content">
                                            <div className="row align-content-center justify-content-center">
                                                <div className="col-9 col-md-8 col-xl-7 col-xxl-6">
                                                    <div className="pxp-featured-posts-item-date">September 5, 2021</div>
                                                    <div className="pxp-featured-posts-item-title">How to start looking for a job</div>
                                                    <div className="pxp-featured-posts-item-summary pxp-text-light mt-2">Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data...</div>
                                                    <div className="mt-4 mt-md-5 text-center">
                                                        <a role="button" onClick={handleBlogDetails} className="btn rounded-pill pxp-section-cta">Read Article<span className="fa fa-angle-right"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="pxp-featured-posts-item pxp-cover" style={{ backgroundImage: "url(images/company-hero-1.jpg)" }}>
                                    <div className="pxp-hero-opacity"></div>
                                    <div className="pxp-featured-posts-item-caption">
                                        <div className="pxp-featured-posts-item-caption-content">
                                            <div className="row align-content-center justify-content-center">
                                                <div className="col-9 col-md-8 col-xl-7 col-xxl-6">
                                                    <div className="pxp-featured-posts-item-date">September 10, 2021</div>
                                                    <div className="pxp-featured-posts-item-title">Resume samples</div>
                                                    <div className="pxp-featured-posts-item-summary pxp-text-light mt-2">Need help writing a resume? Looking for resume examples for specific industries? You’ll find a variety...</div>
                                                    <div className="mt-4 mt-md-5 text-center">
                                                        <a role="button" onClick={handleBlogDetails} className="btn rounded-pill pxp-section-cta">Read Article<span className="fa fa-angle-right"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="carousel-item">
                                <div className="pxp-featured-posts-item pxp-cover" style={{ backgroundImage: "url(images/company-hero-2.jpg)" }}>
                                    <div className="pxp-hero-opacity"></div>
                                    <div className="pxp-featured-posts-item-caption">
                                        <div className="pxp-featured-posts-item-caption-content">
                                            <div className="row align-content-center justify-content-center">
                                                <div className="col-9 col-md-8 col-xl-7 col-xxl-6">
                                                    <div className="pxp-featured-posts-item-date">September 15, 2021</div>
                                                    <div className="pxp-featured-posts-item-title">100 top interview questions - be prepared</div>
                                                    <div className="pxp-featured-posts-item-summary pxp-text-light mt-2">While there are as many different possible interview questions as there are interviewers, it always helps...</div>
                                                    <div className="mt-4 mt-md-5 text-center">
                                                        <a role="button" onClick={handleBlogDetails} className="btn rounded-pill pxp-section-cta">Read Article<span className="fa fa-angle-right"></span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button className="carousel-control-prev" type="button" data-bs-target="#pxp-blog-featured-posts-carousel" data-bs-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Previous</span>
                        </button>
                        <button className="carousel-control-next" type="button" data-bs-target="#pxp-blog-featured-posts-carousel" data-bs-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="visually-hidden">Next</span>
                        </button>
                    </div>

                    <div className="mt-4 mt-lg-5">
                        <div className="row">
                            <div className="col-lg-7 col-xl-8 col-xxl-9">
                                <div className="row">
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">August 31, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Assessments</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">10 awesome free career self assessments</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Figuring out what you want to be when you grow up is hard, but a career test can make it easier to find...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-2.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 5, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Jobs</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">How to start looking for a job</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-3.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 10, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Resume</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">Resume samples</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Need help writing a resume? Looking for resume examples for specific industries? You’ll find a variety...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-4.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 15, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Interview</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">100 top interview questions - be prepared</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">While there are as many different possible interview questions as there are interviewers, it always helps...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">August 31, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Assessments</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">10 awesome free career self assessments</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Figuring out what you want to be when you grow up is hard, but a career test can make it easier to find...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-2.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 5, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Jobs</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">How to start looking for a job</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-3.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 10, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Resume</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">Resume samples</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Need help writing a resume? Looking for resume examples for specific industries? You’ll find a variety...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-4.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">September 15, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Interview</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">100 top interview questions - be prepared</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">While there are as many different possible interview questions as there are interviewers, it always helps...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-posts-card-1-container">
                                        <div className="pxp-posts-card-1 pxp-has-border">
                                            <div className="pxp-posts-card-1-top">
                                                <div className="pxp-posts-card-1-top-bg">
                                                    <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                                    <div className="pxp-posts-card-1-info">
                                                        <div className="pxp-posts-card-1-date">August 31, 2021</div>
                                                        <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-category">Assessments</a>
                                                    </div>
                                                </div>
                                                <div className="pxp-posts-card-1-content">
                                                    <a role="button" onClick={handleBlogDetails} className="pxp-posts-card-1-title">10 awesome free career self assessments</a>
                                                    <div className="pxp-posts-card-1-summary pxp-text-light">Figuring out what you want to be when you grow up is hard, but a career test can make it easier to find...</div>
                                                </div>
                                            </div>
                                            <div className="pxp-posts-card-1-bottom">
                                                <div className="pxp-posts-card-1-cta">
                                                    <a role="button" onClick={handleBlogDetails}>Read more<span className="fa fa-angle-right"></span></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="row mt-4 mt-lg-5 justify-content-between align-items-center">
                                    <div className="col-auto">
                                        <nav className="mt-3 mt-sm-0" aria-label="Blog articles pagination">
                                            <ul className="pagination pxp-pagination">
                                                <li className="page-item active" aria-current="page">
                                                    <span className="page-link">1</span>
                                                </li>
                                                <li className="page-item"><a className="page-link" role="button" onClick={handleBlogDetails}>2</a></li>
                                                <li className="page-item"><a className="page-link" role="button" onClick={handleBlogDetails}>3</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                    <div className="col-auto">
                                        <a role="button" onClick={handleBlogDetails} className="btn rounded-pill pxp-section-cta mt-3 mt-sm-0">Show me more<span className="fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5 col-xl-4 col-xxl-3">
                                <div className="pxp-blogs-list-side-panel">
                                    <h3>Search Articles</h3>
                                    <div className="mt-2 mt-lg-3">
                                        <div className="input-group">
                                            <span className="input-group-text"><span className="fa fa-search"></span></span>
                                            <input type="text" className="form-control" placeholder="Search by keyword" />
                                        </div>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Categories</h3>
                                    <ul className="list-unstyled pxp-blogs-side-list mt-2 mt-lg-3">
                                        <li><a role="button" onClick={handleBlogDetails}>Assessments</a></li>
                                        <li><a role="button" onClick={handleBlogDetails}>Jobs</a></li>
                                        <li><a role="button" onClick={handleBlogDetails}>Resume</a></li>
                                        <li><a role="button" onClick={handleBlogDetails}>Future of Work</a></li>
                                        <li><a role="button" onClick={handleBlogDetails}>Interview</a></li>
                                    </ul>

                                    <h3 className="mt-3 mt-lg-4">Recent Articles</h3>
                                    <ul className="list-unstyled pxp-blogs-side-articles-list mt-2 mt-lg-3">
                                        <li className="mb-3">
                                            <a role="button" onClick={handleBlogDetails} className="pxp-blogs-side-articles-list-item">
                                                <div className="pxp-blogs-side-articles-list-item-fig pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                                <div className="pxp-blogs-side-articles-list-item-content ms-3">
                                                    <div className="pxp-blogs-side-articles-list-item-title">10 awesome free career self assessments</div>
                                                    <div className="pxp-blogs-side-articles-list-item-date pxp-text-light">September 3, 2021</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li className="mb-3">
                                            <a role="button" onClick={handleBlogDetails} className="pxp-blogs-side-articles-list-item">
                                                <div className="pxp-blogs-side-articles-list-item-fig pxp-cover" style={{ backgroundImage: "url(images/post-card-2.jpg)" }}></div>
                                                <div className="pxp-blogs-side-articles-list-item-content ms-3">
                                                    <div className="pxp-blogs-side-articles-list-item-title">How to start looking for a job</div>
                                                    <div className="pxp-blogs-side-articles-list-item-date pxp-text-light">September 5, 2021</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li className="mb-3">
                                            <a role="button" onClick={handleBlogDetails} className="pxp-blogs-side-articles-list-item">
                                                <div className="pxp-blogs-side-articles-list-item-fig pxp-cover" style={{ backgroundImage: "url(images/post-card-3.jpg)" }}></div>
                                                <div className="pxp-blogs-side-articles-list-item-content ms-3">
                                                    <div className="pxp-blogs-side-articles-list-item-title">Resume samples</div>
                                                    <div className="pxp-blogs-side-articles-list-item-date pxp-text-light">September 10, 2021</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li className="mb-3">
                                            <a role="button" onClick={handleBlogDetails} className="pxp-blogs-side-articles-list-item">
                                                <div className="pxp-blogs-side-articles-list-item-fig pxp-cover" style={{ backgroundImage: "url(images/post-card-4.jpg)" }}></div>
                                                <div className="pxp-blogs-side-articles-list-item-content ms-3">
                                                    <div className="pxp-blogs-side-articles-list-item-title">100 top interview questions - be prepared</div>
                                                    <div className="pxp-blogs-side-articles-list-item-date pxp-text-light">September 15, 2021</div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a role="button" onClick={handleBlogDetails} className="pxp-blogs-side-articles-list-item">
                                                <div className="pxp-blogs-side-articles-list-item-fig pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                                <div className="pxp-blogs-side-articles-list-item-content ms-3">
                                                    <div className="pxp-blogs-side-articles-list-item-title">10 awesome free career self assessments</div>
                                                    <div className="pxp-blogs-side-articles-list-item-date pxp-text-light">September 20, 2021</div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>

                                    <h3 className="mt-3 mt-lg-4">Tags</h3>
                                    <div className="mt-2 mt-lg-3 pxp-blogs-side-tags">
                                        <a role="button" onClick={handleBlogDetails}>Work from home</a>
                                        <a role="button" onClick={handleBlogDetails}>Part-time</a>
                                        <a role="button" onClick={handleBlogDetails}>Administration</a>
                                        <a role="button" onClick={handleBlogDetails}>Finance</a>
                                        <a role="button" onClick={handleBlogDetails}>Retail</a>
                                        <a role="button" onClick={handleBlogDetails}>IT</a>
                                        <a role="button" onClick={handleBlogDetails}>Engineering</a>
                                        <a role="button" onClick={handleBlogDetails}>Sales</a>
                                        <a role="button" onClick={handleBlogDetails}>Manufacturing</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    )
}