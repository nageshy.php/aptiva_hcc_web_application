import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import ServicesPageOne from "./servicesPageOne";

export default function ServicesPage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <ServicesPageOne />
            </div>
            <div>
                <FooterPage />
            </div>
        </div>
    )
}