import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import CareersPageOne from "./careersPageOne";

export default function CareersPage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <CareersPageOne />
            </div>
            <div>
                <FooterPage />
            </div>
        </div>
    )
}