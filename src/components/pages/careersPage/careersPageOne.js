import React from "react";
import "./style.css";

export default function CareersPageOne() {

    return (
        <div>
            <section className="pxp-page-header-simple" style={{ backgroundColor: "#ebf6ff" }}>
                <div className="pxp-container">
                    <h1 className="text-center">Search Jobs</h1>
                    <div className="pxp-hero-subtitle pxp-text-light text-center">Search your career opportunity through <strong>12,800</strong> jobs</div>
                </div>
            </section>

            <section className="mt-100">
                <div className="pxp-container">
                    <div className="row">
                        <div className="col-lg-5 col-xl-4 col-xxl-3">
                            <div className="pxp-jobs-list-side-filter">
                                <div className="pxp-list-side-filter-header d-flex d-lg-none">
                                    <div className="pxp-list-side-filter-header-label">Filter Jobs</div>
                                    <a role="button"><span className="fa fa-sliders"></span></a>
                                </div>
                                <div className="mt-4 mt-lg-0 d-lg-block pxp-list-side-filter-panel">
                                    <h3>Search by Keywords</h3>
                                    <div className="mt-2 mt-lg-3">
                                        <div className="input-group">
                                            <span className="input-group-text"><span className="fa fa-search"></span></span>
                                            <input type="text" className="form-control" placeholder="Job Title or Keyword" />
                                        </div>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Location</h3>
                                    <div className="mt-2 mt-lg-3">
                                        <div className="input-group">
                                            <span className="input-group-text"><span className="fa fa-globe"></span></span>
                                            <input type="text" className="form-control" placeholder="Enter location" />
                                        </div>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Category</h3>
                                    <div className="mt-2 mt-lg-3">
                                        <div className="input-group">
                                            <span className="input-group-text"><span className="fa fa-folder-o"></span></span>
                                            <select className="form-select">
                                                <option selected="">All categories</option>
                                                <option>Business Development</option>
                                                <option>Construction</option>
                                                <option>Customer Service</option>
                                                <option>Finance</option>
                                                <option>Healthcare</option>
                                                <option>Human Resources</option>
                                                <option>Marketing &amp; Communication</option>
                                                <option>Project Management</option>
                                                <option>Software Engineering</option>
                                            </select>
                                        </div>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Type of Employment</h3>
                                    <div className="list-group mt-2 mt-lg-3">
                                        <label className="list-group-item d-flex justify-content-between align-items-center pxp-checked">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" checked />
                                                Full Time
                                            </span>
                                            <span className="badge rounded-pill">56</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Part Time
                                            </span>
                                            <span className="badge rounded-pill">34</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Remote
                                            </span>
                                            <span className="badge rounded-pill">24</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Internship
                                            </span>
                                            <span className="badge rounded-pill">27</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Contract
                                            </span>
                                            <span className="badge rounded-pill">76</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Training
                                            </span>
                                            <span className="badge rounded-pill">28</span>
                                        </label>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Experience Level</h3>
                                    <div className="list-group mt-2 mt-lg-3">
                                        <label className="list-group-item d-flex justify-content-between align-items-center">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                No Experience
                                            </span>
                                            <span className="badge rounded-pill">98</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3 pxp-checked">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" checked />
                                                Entry-Level
                                            </span>
                                            <span className="badge rounded-pill">44</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3 pxp-checked">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" checked />
                                                Mid-Level
                                            </span>
                                            <span className="badge rounded-pill">35</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Senior-Level
                                            </span>
                                            <span className="badge rounded-pill">45</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                Manager / Executive
                                            </span>
                                            <span className="badge rounded-pill">21</span>
                                        </label>
                                    </div>

                                    <h3 className="mt-3 mt-lg-4">Salary Range</h3>
                                    <div className="list-group mt-2 mt-lg-3">
                                        <label className="list-group-item d-flex justify-content-between align-items-center pxp-checked">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" checked />
                                                $700 - $1000
                                            </span>
                                            <span className="badge rounded-pill">34</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3 pxp-checked">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" checked />
                                                $1000 - $1200
                                            </span>
                                            <span className="badge rounded-pill">22</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                $1200 - $1400
                                            </span>
                                            <span className="badge rounded-pill">67</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                $1500 - $1800
                                            </span>
                                            <span className="badge rounded-pill">12</span>
                                        </label>
                                        <label className="list-group-item d-flex justify-content-between align-items-center mt-2 mt-lg-3">
                                            <span className="d-flex">
                                                <input className="form-check-input me-2" type="checkbox" value="" />
                                                $2000 - $3000
                                            </span>
                                            <span className="badge rounded-pill">26</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-lg-7 col-xl-8 col-xxl-9">
                            <div className="pxp-jobs-list-top mt-4 mt-lg-0">
                                <div className="row justify-content-between align-items-center">
                                    <div className="col-auto">
                                        <h2><span className="pxp-text-light">Showing</span> 8,536 <span className="pxp-text-light">jobs</span></h2>
                                    </div>
                                    <div className="col-auto">
                                        <select className="form-select">
                                            <option value="0" selected>Most relevant</option>
                                            <option value="1">Newest</option>
                                            <option value="2">Oldest</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div className="row">
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-bullhorn"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Marketing & Communication</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Senior Editor</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>San Francisco, CA
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Artistre Studio</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-1.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-calendar-o"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Project Management</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Software Engineering Team Leader</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>Los Angeles, CA
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Craftgenics</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-2.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-comments-o"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Customer Service</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Techincal Support Engineer</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>Paris, France
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Illuminate Studio</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-3.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-terminal"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Software Engineering</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Javascript Developer</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>London, UK
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Syspresoft</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-4.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-pie-chart"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Business Development</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Technical Writer</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>London, UK
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Gramware</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-5.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-address-card-o"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Human Resources</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Human Resources Coordinator</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>San Francisco, CA
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Bitbytech</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-6.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-terminal"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Software Engineering</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Fullstack Web Developer</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>New York, NY
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">CoderBotics</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-7.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-bullhorn"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Marketing & Communication</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Social Media Expert</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>San Francisco, CA
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Artistre Studio</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-1.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-6 col-lg-12 col-xl-6 col-xxl-4 pxp-jobs-card-1-container">
                                    <div className="pxp-jobs-card-1 pxp-has-border">
                                        <div className="pxp-jobs-card-1-top">
                                            <a href="jobs-list-1.html" className="pxp-jobs-card-1-category">
                                                <div className="pxp-jobs-card-1-category-icon"><span className="fa fa-calendar-o"></span></div>
                                                <div className="pxp-jobs-card-1-category-label">Project Management</div>
                                            </a>
                                            <a href="single-job-1.html" className="pxp-jobs-card-1-title">Software Engineering Team Leader</a>
                                            <div className="pxp-jobs-card-1-details">
                                                <a href="jobs-list-1.html" className="pxp-jobs-card-1-location">
                                                    <span className="fa fa-globe"></span>Los Angeles, CA
                                                </a>
                                                <div className="pxp-jobs-card-1-type">Full-time</div>
                                            </div>
                                        </div>
                                        <div className="pxp-jobs-card-1-bottom">
                                            <div className="pxp-jobs-card-1-bottom-left">
                                                <div className="pxp-jobs-card-1-date pxp-text-light">3 days ago by</div>
                                                <a href="single-company-1.html" className="pxp-jobs-card-1-company">Craftgenics</a>
                                            </div>
                                            <a href="single-company-1.html" className="pxp-jobs-card-1-company-logo" style={{ backgroundImage: "url(images/company-logo-2.png)" }}></a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="row mt-4 mt-lg-5 justify-content-between align-items-center">
                                <div className="col-auto">
                                    <nav className="mt-3 mt-sm-0" aria-label="Jobs list pagination">
                                        <ul className="pagination pxp-pagination">
                                            <li className="page-item active" aria-current="page">
                                                <span className="page-link">1</span>
                                            </li>
                                            <li className="page-item"><a className="page-link" href="#">2</a></li>
                                            <li className="page-item"><a className="page-link" href="#">3</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <div className="col-auto">
                                    <a href="#" className="btn rounded-pill pxp-section-cta mt-3 mt-sm-0">Show me more<span className="fa fa-angle-right"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}