import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import SignUpPageOne from "./signUpPageOne";

export default function SignUpPage() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <SignUpPageOne />
            </div>
            <div>
                <FooterPage />
            </div>

        </div>
    )
}