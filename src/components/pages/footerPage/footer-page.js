import React from 'react';
import "./style.css";
import { useNavigate } from 'react-router';

export default function FooterPage() {

    let navigate = useNavigate();

    function handleHome() {
        navigate('/')
    }

    function handleAbout() {
        navigate('/about-us')
    }

    function handleContact() {
        navigate('/contact-us')
    }
    function handleSignUp() {
        navigate('/sign-up')
    }
    function handleSignIn() {
        navigate('/sign-in')
    }
    function handleServices() {
        navigate('/services')
    }
    function handleCareers() {
        navigate('/careers')
    }
    function handleBlog() {
        navigate('/blog')
    }


    return (
        <div>
            <footer className="pxp-main-footer mt-100">
                <div className="pxp-main-footer-top pt-100 pb-100" style={{ backgroundColor: "#e9f2fb" }}>
                    <div className="pxp-container">
                        <div className="row">
                            <div className="col-lg-6 col-xl-5 col-xxl-3 mb-4">
                                <div className="pxp-footer-logo">
                                    <a onClick={handleHome} className="pxp-animate"><img src="img/logo.png" /></a>
                                </div>
                                {/* <div className="pxp-footer-section mt-3 mt-md-4">
                                    <h3>Call us</h3>
                                    <div className="pxp-footer-phone">+1 7327514647</div>
                                </div> */}
                                <div className="mt-3 mt-md-4 pxp-footer-section">
                                    <div className="pxp-footer-text">
                                        At Aptiva Healthcare, we’ve built a reputation for our expertise in ensuring that our clients get the right hire the first time–every time.
                                        <div className="pxp-services-1-item-cta">
                                            <a onClick={handleAbout} role="button" style={{ color: '#4e950d' }}>Read more<span className="fa fa-angle-right"></span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xl-5 col-xxl-3 mb-4">
                                <div className="pxp-footer-section mt-3 mt-md-4">
                                    <h3>About Us</h3>
                                </div>
                                <div className="mt-3 mt-md-4 pxp-footer-section">
                                    <div className="pxp-footer-text">
                                        <ul className="pxp-footer-list">
                                            <li><a onClick={handleAbout} role="button">About Us</a></li>
                                            <li><a onClick={handleServices} role="button">Services</a></li>
                                            <li><a onClick={handleCareers} role="button">Careers</a></li>
                                            <li><a onClick={handleBlog} role="button">Blog</a></li>
                                            <li><a onClick={handleContact} role="button">Contact Us</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xl-5 col-xxl-3 mb-4">
                                <div className="pxp-footer-section mt-3 mt-md-4">
                                    <h3>Contact us</h3>
                                </div>
                                <div className="mt-3 mt-md-4 pxp-footer-section">
                                    <div className="pxp-footer-text">
                                        <ul className="pxp-footer-list">
                                            <li><a href="#">825 Georges Road, Suite 1</a></li>
                                            <li><a href="#">North Brunswick, NJ 08902</a></li>
                                            <li><a href="#">info@aptivacorp.com</a></li>
                                            <li><a href="#">+1 7327514647</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-xl-5 col-xxl-3 mb-4">
                                <div className="pxp-footer-section mt-3 mt-md-4">
                                    <h3>Follow us</h3>
                                </div>
                                <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3" style={{ display: 'initial' }}>
                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#3b5998" }}
                                        href="#"
                                        role="button"
                                    >
                                        <i className="fa fa-facebook"></i>
                                    </a>

                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#55acee" }}
                                        href="#"
                                        role="button"
                                    ><i className="fa fa-twitter"></i></a>

                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#dd4b39" }}
                                        href="#"
                                        role="button"
                                    ><i className="fa fa-google"></i></a>

                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#ac2bac" }}
                                        href="#"
                                        role="button"
                                    ><i className="fa fa-instagram"></i></a>

                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#0082ca" }}
                                        href="#"
                                        role="button"
                                    ><i className="fa fa-linkedin"></i></a>
                                    <a
                                        className="btn btn-primary btn-floating m-1"
                                        style={{ backgroundColor: "#333333" }}
                                        href="#"
                                        role="button"
                                    ><i className="fa fa-github"></i></a>
                                </div>
                                {/* </div> */}
                                {/* </div> */}
                                {/* </div> */}
                            </div>

                        </div>
                    </div>
                </div>
            </footer>
        </div>
    )
}