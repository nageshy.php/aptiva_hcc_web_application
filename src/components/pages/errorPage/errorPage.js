import React from "react";
import "./style.css";
import { useNavigate } from 'react-router';

export default function ErrorPage() {
    let navigate = useNavigate();

    function handleHome() {
        navigate('/')
    }
    return (
        <div>
            <section className="mt-100 pxp-no-hero">
                <div className="pxp-container">
                    <h2 className="pxp-section-h2 text-center">This page is off the map</h2>
                    <p className="pxp-text-light text-center">We can't seem to find the page you're looking for.</p>

                    <div className="pxp-404-fig text-center mt-4 mt-lg-5">
                        <img src="images/404.png" alt="Page not found" />
                    </div>

                    <div className="mt-4 mt-lg-5 text-center">
                        <a role="button" onClick={handleHome} className="btn rounded-pill pxp-section-cta">Go Home<span className="fa fa-angle-right"></span></a>
                    </div>
                </div>
            </section>
        </div>
    )
}