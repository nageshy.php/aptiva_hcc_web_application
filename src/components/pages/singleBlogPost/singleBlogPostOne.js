import React from "react";
import "./style.css";

export default function SingleBlogPostOne() {

    return (
        <div>
            <section>
                <div className="pxp-container">
                    <div className="pxp-blog-hero">
                        <div className="row justify-content-between align-items-end">
                            <div className="col-lg-8 col-xxl-6">
                                <h1>How to Start Looking for a New Job</h1>
                                <div className="pxp-hero-subtitle pxp-text-light">Have realistic expectations Most designers will tell you that, as much as we all love to watch home design shows, their prevalence has done them a bit of a disservice.</div>
                            </div>
                            <div className="col-lg-4 col-xxl-6">
                                <div className="text-start text-lg-end mt-4 mt-lg-0">
                                    <div className="pxp-single-blog-top-category">
                                        Posted in <a href="#">Future of Work</a>
                                    </div>
                                    <div className="pxp-single-blog-top-author">
                                        by <a href="#">Wade Warren</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <img className="pxp-single-blog-featured-img" src="images/single-blog-post.jpg" alt="How to Start Looking for a New Job" />
                </div>
            </section>
            <section className="mt-100">
                <div className="pxp-container">
                    <div className="row justify-content-center">
                        <div className="col-xl-7">
                            <div className="pxp-single-blog-content">
                                <h2>Let the Perfect Job Find You</h2>
                                <p>Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data, and double-checked by an expert. If it's none of these things, stop right here and learn how to get your resume ready for a job search.</p>
                                <p>So you're ready to find your next job. And Jobster career expert Vicki Salemi, who spent more than 15 years in corporate recruiting, says now is as good of a time as any to start looking for it. "Companies hire year-round," she says. "You never know when the right position will open up." A Jobster poll revealed that 90% of respondents are ready to reset their job search in September, whereas only 10% are willing to wait until the beginning of the new year.</p>
                                <p>But with more than 5 million jobs on Jobster, where do you even begin? Follow our tips and tricks below to help you find better, faster.</p>
                                <h2 className="mt-4 mt-lg-5">Get Your Resume Done Right</h2>
                                <p>Your resume is key to kicking off a strong job search. Not sure how yours stacks up? Let the experts at Jobster create your resume. Our resume writers will conduct a comprehensive review of your experience and career goals to craft a resume that reflects your awesomeness.</p>
                                <blockquote className="blockquote-style-one mb-5 mt-5">
                                    <p>"I learned by asking questions and by old-fashioned trial and error, with a healthy dose of over-communication."</p>
                                    <cite>Chuck Robbins</cite>
                                </blockquote>
                                <img src="images/single-blog-post-1.jpg" alt="Get Your Resume Done Right" className="mt-4 mt-lg-5" />
                                <h2 className="mt-4 mt-lg-5">Get Your Resume Done Right</h2>
                                <ul>
                                    <li>But with more than 5 million jobs on Jobster</li>
                                    <li>Follow our tips and tricks below to help you find better</li>
                                    <li>Your resume is key to kicking off a strong job search</li>
                                    <li>Let the experts at Jobster create your resume</li>
                                </ul>
                                <p>Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data, and double-checked by an expert. If it's none of these things, stop right here and learn how to get your resume ready for a job search.</p>
                            </div>

                            <div className="mt-100">
                                <div className="pxp-single-blog-share">
                                    <span className="me-4">Share this article</span>
                                    <ul className="list-unstyled">
                                        <li><a href="#"><span className="fa fa-facebook"></span></a></li>
                                        <li><a href="#"><span className="fa fa-twitter"></span></a></li>
                                        <li><a href="#"><span className="fa fa-pinterest-p"></span></a></li>
                                        <li><a href="#"><span className="fa fa-linkedin"></span></a></li>
                                    </ul>
                                </div>
                            </div>

                            <div className="mt-100">
                                <div className="pxp-blog-comments-block">
                                    <div className="pxp-blog-post-comments">
                                        <h4>2 Comments</h4>
                                        <div className="mt-3 mt-lg-4">
                                            <ol className="pxp-comments-list">
                                                <li>
                                                    <div className="pxp-comments-list-item">
                                                        <img src="images/avatar-1.jpg" alt="Melvin Blackwell" />
                                                        <div className="pxp-comments-list-item-body">
                                                            <h5>Melvin Blackwell</h5>
                                                            <div className="pxp-comments-list-item-date">November 6, 2021 at 9:10 am</div>
                                                            <div className="pxp-comments-list-item-content mt-2">Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data, and double-checked by an expert. If it's none of these things, stop right here and learn how to get your resume ready for a job search.</div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li className="mt-3 mt-lg-4">
                                                    <div className="pxp-comments-list-item">
                                                        <img src="images/avatar-2.jpg" alt="Wade Warren" />
                                                        <div className="pxp-comments-list-item-body">
                                                            <h5>Wade Warren</h5>
                                                            <div className="pxp-comments-list-item-date">November 6, 2021 at 9:10 am</div>
                                                            <div className="pxp-comments-list-item-content mt-2">Your resume is key to kicking off a strong job search. Not sure how yours stacks up? Let the experts at Jobster create your resume. </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ol>
                                        </div>
                                        <div className="mt-3 mt-lg-4">
                                            <h4>Leave a Reply</h4>
                                            <form className="pxp-comments-form">
                                                <div className="pxp-comments-logged-as">
                                                    Logged in as Melvin Blackwell. <a href="#">Log out?</a>
                                                </div>
                                                <div className="mt-3 mt-lg- mb-3">
                                                    <label htmlFor="pxp-comments-comment" className="form-label">Comment</label>
                                                    <textarea className="form-control" id="pxp-comments-comment" placeholder="Type your comment here..."></textarea>
                                                </div>
                                                <button className="btn rounded-pill pxp-section-cta">Post Comment</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}