import React from "react";
import "./style.css";

export default function SingleBlogPostTwo() {

    return (
        <div>
            <section className="mt-100">
                <div className="pxp-container">
                    <h2 className="pxp-subsection-h2">Related Articles</h2>
                    <p className="pxp-text-light">Browse the latest career advices</p>

                    <div className="row mt-3 mt-md-4">
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                            <div className="pxp-posts-card-1 pxp-has-border">
                                <div className="pxp-posts-card-1-top">
                                    <div className="pxp-posts-card-1-top-bg">
                                        <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-1.jpg)" }}></div>
                                        <div className="pxp-posts-card-1-info">
                                            <div className="pxp-posts-card-1-date">August 31, 2021</div>
                                            <a href="#" className="pxp-posts-card-1-category">Assessments</a>
                                        </div>
                                    </div>
                                    <div className="pxp-posts-card-1-content">
                                        <a href="#" className="pxp-posts-card-1-title">10 awesome free career self assessments</a>
                                        <div className="pxp-posts-card-1-summary pxp-text-light">Figuring out what you want to be when you grow up is hard, but a career test can make it easier to find...</div>
                                    </div>
                                </div>
                                <div className="pxp-posts-card-1-bottom">
                                    <div className="pxp-posts-card-1-cta">
                                        <a href="#">Read more<span className="fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                            <div className="pxp-posts-card-1 pxp-has-border">
                                <div className="pxp-posts-card-1-top">
                                    <div className="pxp-posts-card-1-top-bg">
                                        <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-2.jpg)" }}></div>
                                        <div className="pxp-posts-card-1-info">
                                            <div className="pxp-posts-card-1-date">September 5, 2021</div>
                                            <a href="#" className="pxp-posts-card-1-category">Jobs</a>
                                        </div>
                                    </div>
                                    <div className="pxp-posts-card-1-content">
                                        <a href="#" className="pxp-posts-card-1-title">How to start looking for a job</a>
                                        <div className="pxp-posts-card-1-summary pxp-text-light">Your resume is perfect. It's keyword-optimized, industry-specified, full of achievements, backed by data...</div>
                                    </div>
                                </div>
                                <div className="pxp-posts-card-1-bottom">
                                    <div className="pxp-posts-card-1-cta">
                                        <a href="#">Read more<span className="fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                            <div className="pxp-posts-card-1 pxp-has-border">
                                <div className="pxp-posts-card-1-top">
                                    <div className="pxp-posts-card-1-top-bg">
                                        <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-3.jpg)" }}></div>
                                        <div className="pxp-posts-card-1-info">
                                            <div className="pxp-posts-card-1-date">September 10, 2021</div>
                                            <a href="#" className="pxp-posts-card-1-category">Resume</a>
                                        </div>
                                    </div>
                                    <div className="pxp-posts-card-1-content">
                                        <a href="#" className="pxp-posts-card-1-title">Resume samples</a>
                                        <div className="pxp-posts-card-1-summary pxp-text-light">Need help writing a resume? Looking for resume examples for specific industries? You’ll find a variety...</div>
                                    </div>
                                </div>
                                <div className="pxp-posts-card-1-bottom">
                                    <div className="pxp-posts-card-1-cta">
                                        <a href="#">Read more<span className="fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-xl-4 col-xxl-3 pxp-posts-card-1-container">
                            <div className="pxp-posts-card-1 pxp-has-border">
                                <div className="pxp-posts-card-1-top">
                                    <div className="pxp-posts-card-1-top-bg">
                                        <div className="pxp-posts-card-1-image pxp-cover" style={{ backgroundImage: "url(images/post-card-4.jpg)" }}></div>
                                        <div className="pxp-posts-card-1-info">
                                            <div className="pxp-posts-card-1-date">September 15, 2021</div>
                                            <a href="#" className="pxp-posts-card-1-category">Interview</a>
                                        </div>
                                    </div>
                                    <div className="pxp-posts-card-1-content">
                                        <a href="#" className="pxp-posts-card-1-title">100 top interview questions - be prepared</a>
                                        <div className="pxp-posts-card-1-summary pxp-text-light">While there are as many different possible interview questions as there are interviewers, it always helps...</div>
                                    </div>
                                </div>
                                <div className="pxp-posts-card-1-bottom">
                                    <div className="pxp-posts-card-1-cta">
                                        <a href="#">Read more<span className="fa fa-angle-right"></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}