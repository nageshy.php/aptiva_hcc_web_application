import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import SingleBlogPostOne from "./singleBlogPostOne";
import SingleBlogPostTwo from "./singleBlogPostTwo";

export default function SingleBlog() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <SingleBlogPostOne />
            </div>
            <div>
                <SingleBlogPostTwo />
            </div>
            <div>
                <FooterPage />
            </div>
        </div>
    )
}