import React from "react";
import "./style.css";
import HeaderPage from "../headerPage/header-page";
import FooterPage from "../footerPage/footer-page";
import SignInPageOne from "./signUpPageOne";

export default function SignIn() {

    return (
        <div>
            <div>
                <HeaderPage />
            </div>
            <div>
                <SignInPageOne />
            </div>
            <div>
                <FooterPage />
            </div>
        </div>
    )
}