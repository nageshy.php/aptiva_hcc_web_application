import React from "react";
import "./style.css";

export default function SignInPageOne() {

    return (
        <div>
            <section className="pxp-hero vh-100">
                <div className="row align-items-center pxp-sign-hero-container">
                    <div className="col-xl-6 pxp-column">
                        <div className="pxp-sign-hero-fig text-center pb-100 pt-100">
                            <img src="images/signin-big-fig.png" alt="Sign In" />
                            <h1 className="mt-4 mt-lg-5">Welcome back!</h1>
                        </div>
                    </div>
                    <div className="col-xl-6 pxp-column pxp-is-light">
                        <div className="pxp-sign-hero-form pb-100 pt-100">
                            <div className="row justify-content-center">
                                <div className="col-lg-6 col-xl-7 col-xxl-5">
                                    <div className="pxp-sign-hero-form-content">
                                        <h5 className="text-center">Sign In</h5>
                                        <form className="mt-4">
                                            <div className="form-floating mb-3">
                                                <input type="email" className="form-control" id="pxp-signin-page-email" placeholder="Email address" />
                                                <label htmlFor="pxp-signin-page-email">Email address</label>
                                                <span className="fa fa-envelope-o"></span>
                                            </div>
                                            <div className="form-floating mb-3">
                                                <input type="password" className="form-control" id="pxp-signin-page-password" placeholder="Password" />
                                                <label htmlFor="pxp-signin-page-password">Password</label>
                                                <span className="fa fa-lock"></span>
                                            </div>
                                            <a href="#" className="btn rounded-pill pxp-sign-hero-form-cta">Continue</a>
                                            <div className="mt-4 text-center pxp-sign-hero-form-small">
                                                <a href="#" className="pxp-modal-link">Forgot password</a>
                                            </div>
                                            <div className="mt-4 text-center pxp-sign-hero-form-small">
                                                New to Jobster? <a href="sign-up.html">Create an account</a>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}