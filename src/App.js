import './App.css';
import {
  BrowserRouter as Router,
  Route,
  Routes,
} from 'react-router-dom';
import HomePage from "./components/pages/homePage/index";
import AboutUs from "./components/pages/aboutUsPage/index";
import ContactUsPage from "./components/pages/contactUsPage/index";
import SignUpPage from "./components/pages/signUpPage/index";
import SignIn from "./components/pages/signInPage/index";
import ServicesPage from "./components/pages/servicesPage/index";
import CareersPage from "./components/pages/careersPage/index";
import BlogPage from "./components/pages/blogPage/index";
import ErrorPage from "./components/pages/errorPage/errorPage";
import SingleBlog from "./components/pages/singleBlogPost/index";

function App() {
  return (
    <div className="App">
      <Router>
        <Routes>
          <Route exact path='/' element={<HomePage />}>
            {({ match }) => <HomePage match={match} />}
          </Route>
          <Route exact path='/about-us' element={<AboutUs />}>
            {({ match }) => <AboutUs match={match} />}
          </Route>
          <Route exact path='/contact-us' element={<ContactUsPage />}>
            {({ match }) => <ContactUsPage match={match} />}
          </Route>
          <Route exact path='/sign-up' element={<SignUpPage />}>
            {({ match }) => <SignUpPage match={match} />}
          </Route>
          <Route exact path='/sign-in' element={<SignIn />}>
            {({ match }) => <SignIn match={match} />}
          </Route>
          <Route exact path='/services' element={<ServicesPage />}>
            {({ match }) => <ServicesPage match={match} />}
          </Route>
          <Route exact path='/careers' element={<CareersPage />}>
            {({ match }) => <CareersPage match={match} />}
          </Route>
          <Route exact path='/blog' element={<BlogPage />}>
            {({ match }) => <BlogPage match={match} />}
          </Route>
          <Route exact path='/blog-details' element={<SingleBlog />}>
            {({ match }) => <SingleBlog match={match} />}
          </Route>
          <Route exact path='*' element={<ErrorPage />}>
            {({ match }) => <ErrorPage match={match} />}
          </Route>
        </Routes>
      </Router>
    </div>
  );
}

export default App;
